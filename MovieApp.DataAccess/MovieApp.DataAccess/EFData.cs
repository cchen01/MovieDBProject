﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp.DataAccess
{
  public class EFData
  {
    MovieDBEntities db = new MovieDBEntities();
    public List<Movie> GetAllMovies()
    {
      return db.Movies.ToList();
    }
    public bool InsertMovie(Movie m)
    {
      try
      {
        db.Movies.Add(m);
        db.SaveChanges();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public bool DeleteMovie(int id)
    {
      Movie e = db.Movies.Find(id);
      try
      {
        //db.Entry(e).State = EntityState.Deleted;
        db.Movies.Remove(e);
        db.SaveChanges();
        return true;
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message);
        return false;
      }
    }
    public bool FindMovieActor(string actor)
    {
      foreach (var item in db.Movies.ToList())
      {
        if (item.Actor == actor)
        {
          return true;
        }
      }
      return false;
    }
    public bool FindMovieTitle(string title)
    {
      foreach (var item in db.Movies.ToList())
      {
        if (item.Title == title)
        {
          return true;
        }
      }
      return false;
    }
    public bool FindMovieAud(string aud)
    {
      foreach (var item in db.Movies.ToList())
      {
        if (item.Audience == aud)
        {
          return true;
        }
      }
      return false;
    }
  }
}