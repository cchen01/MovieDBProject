﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Xml.Linq;
using System.Threading.Tasks;
using MovieApp.DataAccess;

namespace MovieApp.ConsoleClient
{
  public class Program
  {
    public static void Main(string[] args)
    {
      SearchMovieActor();
      PrintAllMovies();
      Console.ReadLine();
    }
    public static void PrintAllMovies()
    {
      EFData ef = new EFData();
      List<Movie> movies = ef.GetAllMovies();
      foreach (var i in movies)
      {
        Console.WriteLine("{0} {1}", i.Title, i.Audience);
      }
    }

    public static void InsertMovie()
    {
      EFData ef = new EFData();
      Movie e = new Movie()
      {
        Title="Sherlock Holmes I",
        Actor="Yokoshima Toshinoya",
        Audience="PG-13"
      };
      bool b = ef.InsertMovie(e);
    }
    public static void DeleteMovie()
    {
      EFData ef = new EFData();

      bool b = ef.DeleteMovie(4);
      if (b == true)
      {
        Console.WriteLine("Deleted");
      }
      else
      {
        Console.WriteLine("Not deleted");
      }
    }
    public static void SearchMovieTitle()
    {
      var data = new EFData();
      bool b=data.FindMovieTitle("Osmosis Jones");
      if (b)
      {
        Console.WriteLine("Found it");
      }
      else
      {
        Console.WriteLine("Did not find it.");
      }
    }
    public static void SearchMovieAud()
    {
      var data = new EFData();
      bool b = data.FindMovieAud("G");
      if (b)
      {
        Console.WriteLine("Found it.");
      }
      else
      {
        Console.WriteLine("Did not find it.");
      }
    }
    public static void SearchMovieActor()
    {
      var data = new EFData();
      bool b = data.FindMovieActor("Chris Rock");
      if (b)
      {
        Console.WriteLine("Found it.");
      }
      else
      {
        Console.WriteLine("Did not find it.");
      }
    } 
  }
}
