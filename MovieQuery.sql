create database MovieDB
go

use MovieDB
go

create table Movie
(
MovieID int not null identity(1,1) primary key,
Title nvarchar(50) not null,
Audience nvarchar(50) not null,
Actor nvarchar(50) not null,
);


insert into Movie
(Title,Audience,Actor)
values ('Madagascar','G','Ivy Jeans');

delete from Movie
where Title='The 13 Ghosts';